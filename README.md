# crawl-book

#### 介绍
crawl-book一个相对独立的小说爬虫程序，为[《小说精品屋》](https://www.oschina.net/p/fiction_house)项目的小说爬虫程序源码，主要作用是抓取网络小说到数据库中，可配合[《小说精品屋》](https://www.oschina.net/p/fiction_house)项目使用，也可独立使用或者用于其他的项目中。只要你的硬盘足够大，该爬虫程序理论上是可以无限制地爬取网络小说的 ，大家根据自己的需求，爬取自己需要的小说数量。

小说精品屋是一个功能完善的小说弹幕网站，包含精品小说专区和轻小说专区。包括小说分类、小说搜索、小说排行、完本小说、小说评分、小说在线阅读、小说书架、阅读记录、小说下载、小说弹幕、小说自动爬取、小说内容自动分享到微博、邮件自动推广、链接自动推送到百度搜索引擎等功能。包含电脑端、移动端、微信小程序等多个平台，现已开源web端和微信小程序端源码，大家可以用于学习或者商用。 

#### 软件架构
springboot+mybatis


#### 安装教程

1.  新建Mysql数据库books
2.  执行sql/book.sql文件
3.  修改application.yml文件的数据库配置
4.  运行程序即可

#### 源码地址

[小说阅读弹幕网站 小说精品屋源码-开源中国](https://www.oschina.net/p/fiction_house)

[小说精品屋-微信小程序源码-开源中国](https://www.oschina.net/p/novel_boutique__wechat_applet)

